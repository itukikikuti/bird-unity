﻿using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    const float blockSpace = 4f;
    const float speed = 0.04f;

    float lastBlockPosition = 0f;
    float distance = 0f;
    float fallingSpeed = 0f;
    int score = 0;

    [SerializeField] TextMesh scoreText;
    [SerializeField] AudioSource audioSouce;
    [SerializeField] AudioClip flySound;
    [SerializeField] AudioClip pointSound;
    [SerializeField] GameObject blockPrefab;

    void Update()
    {
        if (lastBlockPosition < 10f)
        {
            lastBlockPosition += blockSpace;

            GameObject clone = Instantiate(blockPrefab);
            clone.transform.position = new Vector3(lastBlockPosition, Random.Range(-2f, 2f), 0f);
        }

        lastBlockPosition -= speed;
        fallingSpeed -= 0.01f;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            fallingSpeed = 0.2f;
            AudioSource.PlayClipAtPoint(flySound, transform.position);
        }

        transform.position += new Vector3(0f, fallingSpeed, 0f);
        transform.eulerAngles = new Vector3(0f, 0f, fallingSpeed * 100f);

        distance += speed;

        if (distance > blockSpace)
        {
            distance -= blockSpace;
            score++;
            scoreText.text = score.ToString();
            AudioSource.PlayClipAtPoint(pointSound, transform.position);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        SceneManager.LoadScene("Title");
    }
}
