﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    const float speed = 0.04f;

    void Update()
    {
        transform.position -= new Vector3(speed, 0f, 0f);

        if(transform.position.x < -10f)
        {
            Destroy(gameObject);
        }
    }
}
